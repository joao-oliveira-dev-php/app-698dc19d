<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Http\Requests\StoreProductRequest;

class ProductController extends Controller
{

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function store(StoreProductRequest $request)
    {
        try {
            $data = $request->all();
            $product = $this->productService->store($data);
            return response()->json($product, 201);
        } catch (Exception $e) {
            return response()->json(['errors' => ['Erro ao criar produto']], 400);
        }
    }

}
