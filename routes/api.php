<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductMovementController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('products')->group(function () {
    Route::post('/',[ProductController::class, 'store']);
});

Route::prefix('product_movements')->group(function () {
    Route::get('/',[ProductMovementController::class, 'index']);
    Route::post('/',[ProductMovementController::class, 'store']);
});
