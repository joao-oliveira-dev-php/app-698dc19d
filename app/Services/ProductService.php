<?php
namespace App\Services;

use App\Repositories\ProductRepository;

class ProductService
{

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function store($data) {
        $product = $this->productRepository->store($data);
        return $product;
    }
}