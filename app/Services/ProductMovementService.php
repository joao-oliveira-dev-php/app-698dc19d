<?php

namespace App\Services;

use App\Repositories\ProductMovementRepository;

class ProductMovementService
{

  public function __construct(ProductMovementRepository $productMovementRepository)
  {
    $this->productMovementRepository = $productMovementRepository;
  }

  public function index()
  {
    $productMovements = $this->productMovementRepository->index();
    return $productMovements;
  }

  public function store($data) {
    $productMovement = $this->productMovementRepository->store($data);
    return $productMovement;
  }

}
