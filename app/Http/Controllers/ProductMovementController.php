<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductMovementRequest;
use App\Services\ProductMovementService;
use Illuminate\Http\Request;
use Exception;

class ProductMovementController extends Controller
{
    public function __construct(ProductMovementService $productMovementService)
    {
        $this->productMovementService = $productMovementService;
    }

    public function index()
    {
        try {
            $productMovements = $this->productMovementService->index();
            return response()->json($productMovements, 200);
        } catch (Exception $e) {
            return response()->json(['errors' => ['Erro ao listar movimentações']], 400);
        }
    }

    public function store(StoreProductMovementRequest $request)
    {
        try {
            $data = $request->all();
            $productMovement = $this->productMovementService->store($data);
            return response()->json($productMovement, 201);
        } catch (Exception $e) {
            return response()->json(['errors' => ['Erro ao criar movimentação']], 400);
        }
    }
}
