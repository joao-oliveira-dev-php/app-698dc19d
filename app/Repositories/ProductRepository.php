<?php
namespace App\Repositories;

use App\Models\Product;

class ProductRepository
{
    // Criar novo produto
    public function store($data) {
        $product = new Product();
        $product->name = $data['name'];
        $product->sku = $data['sku'];
        $product->initial_inventory = $data['initial_inventory'];
        $product->save();
        return $product;
    }

    // Listar produto pelo SKU
    public function getBySku($sku) {
        return Product::where('sku', $sku)->first();
    }

}
