<?php
namespace App\Repositories;

use App\Models\Product;
use App\Models\ProductMovement;

class ProductMovementRepository
{

  public function __construct(ProductRepository $productRepository)
  {
    $this->productRepository = $productRepository;
  }

  public function index() {
    // Listar todas movimentações
    return ProductMovement::all();
  }

  public function store($data) {
    // Buscar produto pelo SKU
    $product = $this->productRepository->getBySku($data['sku']);

    // Salvar movimentação do produto
    $productMovement = new ProductMovement();
    $productMovement->product_id = $product['id'];
    $productMovement->sku = $data['sku'];
    $productMovement->quantity = $data['quantity'];
    $productMovement->save();
    return $productMovement;
  }

}
