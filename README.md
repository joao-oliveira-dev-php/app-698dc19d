<h1 align="center"> AppMax API </h1>

<img src="https://uploaddeimagens.com.br/images/004/185/346/full/download.png?1669682853" alt="AppMax">

# Padrão de projeto utilizado

### - Padrão API REST
### - SERVICE - REPOSITORY

# Documentação

### - [Documentação (Postman)](https://documenter.getpostman.com/view/12799523/2s8YsxvBt5)


## 🚀 Versão das tecnologias

Este projeto foi desenvolvido com as seguintes versões:


- PHP: ^7.3|^8.0

- Laravel: ^8.75



## ⚙ Passo a passo para rodar o projeto

1- Clonar o projeto:
> git clone https://gitlab.com/joao-oliveira-dev-php/app-698dc19d.git

2- Criar o .env com base no example:
> copy .env

3- Instalar as dependencias via composer:
> composer install

4- Gerar a key:
> php artisan key:generate

5- Criar o DB:
> Criar DB com o nome "laravel"

6- Rodar as migrations:
> php artisan migrate

7- Para iniciar a aplicação:
> php artisan serve



Feito com 💜 por JOÃO OLIVEIRA 👋 [Veja meu Linkedin](https://www.linkedin.com/in/joao-php/)
<br>

